class CreateIntakes < ActiveRecord::Migration[5.0]
  def change
    create_table :intakes do |t|
      t.string :uploader
      t.string :filename
      t.string :file_md5
      t.string :result
      t.string :result_description

      t.timestamps
    end
  end
end
