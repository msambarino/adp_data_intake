require 'nokogiri'

module ADP
  module Parser

    ADP_SCHEMA_FILE = File.join(Rails.root, 'app', 'adp', 'ADP.xsd')
    SCHEMA = Nokogiri::XML::Schema(File.open(ADP_SCHEMA_FILE))

    def self.parse(filepath)
      doc = Nokogiri::XML(File.open(filepath))
      employees = Hash.new
      doc.xpath('//ROW').each do |row|
        employee = {
          'first_name' => row.xpath('FIRST_NAME').text,
          'middle_name' => row.xpath('FIRST_NAME').text,
          'last_name' => row.xpath('LAST_NAME').text,
          'email' => row.xpath('WORK_E-MAIL').text,
          'job_title' => row.xpath('JOB_TITLE').text,
          'eyr_job_title' => row.xpath('EYR_JOB_TITLE').text,
          'birthdate' => row.xpath('DATE_OF_BIRTH').text,
          'country' => row.xpath('COUNTRY').text,
          'location' => row.xpath('LOCATION_NAME').text,
          'nationality' => row.xpath('NATIONALITY').text,
          'passport' => row.xpath('PASSPORT_').text,
          'city' => row.xpath('CITY').text,
          'street_address' => row.xpath('STREET_ADDRESS').text
        }

        employees[employee["email"]] = employee

      end

      return employees.values
    end

    def self.validate_xml_doc(doc)
      return doc.errors.empty? && SCHEMA.valid?(doc)
    end
  end
end
