require 'pg'

module ADP
  module Persistence

    EMPLOYEE_INSERT_STATEMENT = "insert into employees(email, first_name, middle_name, last_name, birthdate, country, city, location, created_at, updated_at) values ($1, $2, $3, $4, $5, $6, $7, $8, now(), now())"

    def self.persist(employees)
      begin
        conn = PG::Connection.open(dbname: ENV["ADP_DB_NAME"],
                                   user: ENV["ADP_DB_USER"],
                                   password: ENV["ADP_DB_PASSWORD"],
                                   host: ENV["ADP_DB_HOST"],
                                   port: ENV["ADP_DB_PORT"])

        conn.prepare("insert_employee", EMPLOYEE_INSERT_STATEMENT)
        conn.transaction do |conn|
          insert_employees(conn, employees)
        end
        return {result: "OK"}
      rescue PG::Error => e
        return {result: "ERROR", description: e.message}
      ensure
        conn.close if conn
      end
    end

    def self.insert_employees(conn, employees)
      employees.each do |employee|
        values = [
          employee["email"],
          employee["first_name"],
          employee["middle_name"],
          employee["last_name"],
          employee["birthdate"],
          employee["country"],
          employee["city"],
          employee["location"]
        ]

        conn.exec_prepared("insert_employee", values)
      end
    end

  end

end
