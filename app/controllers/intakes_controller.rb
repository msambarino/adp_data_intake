require 'parser'
require 'persistence'

class IntakesController < ApplicationController
  before_action :set_intake, only: [:show, :destroy]

  # GET /intakes
  def index
    @intakes = Intake.all

    render json: @intakes
  end

  # GET /intakes/1
  def show
    render json: @intake
  end

  # POST /intakes
  def create
    file = adp_data_file
    adp_data = ADP::Parser.parse(file.tempfile.path)
    intake_result = ADP::Persistence.persist(adp_data)
    md5 = Digest::MD5.file(file.tempfile.path).hexdigest

    @intake = Intake.new({
      result: intake_result[:result],
      result_description: intake_result[:description],
      filename: file.original_filename,
      file_md5: md5
    })

    if @intake.save
      render json: @intake, status: :created, location: @intake
    else
      render json: @intake.errors, status: :unprocessable_entity
    end
  end

  # DELETE /intakes/1
  def destroy
    @intake.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_intake
      @intake = Intake.find(params[:id])
    end

    def adp_data_file
      params.require(:file)
    end
end
