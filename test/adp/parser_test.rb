require 'test_helper'
require 'parser'

class ParserTest < ActiveSupport::TestCase

  test "it accepts a valid xml file with the correct schema" do
    file = File.open(Rails.root.join('test', 'adp', 'files', 'valid.xml'))
    doc = Nokogiri::XML(file)
    valid = ADP::Parser.validate_xml_doc(doc)

    assert_equal(true, valid)
  end

  test "it rejects an invalid xml file" do
    file = File.open(Rails.root.join('test', 'adp', 'files', 'invalid.xml'))
    doc = Nokogiri::XML(file)
    valid = ADP::Parser.validate_xml_doc(doc)

    assert_equal(false, valid)
  end

  test "it rejects a valid xml file with an incorrect schema" do
    file = File.open(Rails.root.join('test', 'adp', 'files', 'incorrect_schema.xml'))
    doc = Nokogiri::XML(file)
    valid = ADP::Parser.validate_xml_doc(doc)

    assert_equal(false, valid)
  end
end
