require 'test_helper'

class IntakesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @intake = intakes(:one)
  end

  test "should get index" do
    get intakes_url, as: :json
    assert_response :success
  end

  test "should create intake" do
    assert_difference('Intake.count') do
      file = fixture_file_upload('files/valid.xml', 'application/xml')
      post intakes_url, params: { file: file}
    end

    assert_response 201
  end

  test "should destroy intake" do
    assert_difference('Intake.count', -1) do
      delete intake_url(@intake), as: :json
    end

    assert_response 204
  end
end
